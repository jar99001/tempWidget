package com.allander.tempapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*
public class RestConnection {
    private String userName, email, restUsername, restPassword;
    private Boolean waitingForData = false;
    //MainActivity.processingData func;
    PersonalInfo personalInfo;
    LoginFragment.LoggedInCorrect postFunc;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRestUsername() {
        return restUsername;
    }

    public void setRestUsername(String restUsername) {
        this.restUsername = restUsername;
    }
    public String getRestPassword() {
        return restPassword;
    }
    public void setRestPassword(String restPassword) {
        this.restPassword = restPassword;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }
    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }


    RestConnection(final String restUsername_, String restPassword_, PersonalInfo personalInfo, Consumer<>) {
        this.restUsername = restUsername_;
        this.restPassword = restPassword_;
        this.personalInfo = personalInfo;
        this.postFunc = postFunc;

        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(restUsername_, restPassword.toCharArray());
            }
        });

    }
*/


    class RestPost extends AsyncTask<String, Void, String> {
        private AsyncResponse delegate = null;

        public interface AsyncResponse {
            void processFinish(String output);
        }

        public RestPost(AsyncResponse delegate){
            this.delegate = delegate;
        }

        @Override
        protected String doInBackground(final String... params) {

            String REST_data = "";

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                httpURLConnection.setRequestMethod("POST");

                httpURLConnection.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes(params[1]);
                Log.i("TAG",params[1]);

                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    REST_data += current;
                }
            } catch (Exception e) {
                Log.d("TAG","Error logging in: " + e.getMessage());
                //e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return REST_data;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("TAG","PostData: " + result);
            delegate.processFinish(result);
        }
    }



class RestGet extends AsyncTask<String, Void, String> {
    private AsyncResponse delegate = null;

    public interface AsyncResponse {
        void processFinish(String output) throws JSONException;
    }

    RestGet(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(final String... params) {

        String REST_data = "";

        HttpURLConnection httpURLConnection = null;

        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestMethod("GET");

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                REST_data += current;
            }
            int status = httpURLConnection.getResponseCode();
            switch(status) {
                case 200:Log.d("GEO","Response code 200, OK");break;
                default:Log.d("GEO","No case for response code: " + status);
            }
        } catch (Exception e) {
            Log.d("TAG","Error logging in: " + e.getMessage());
            //e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        return REST_data;
    }

    @Override
    protected void onPostExecute(String result) {
        try {
            delegate.processFinish(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(!result.isEmpty()) {
//                try {
                Log.d("TAG","PostData: " + result);
//                    JSONObject jData = new JSONObject(result);
//                    personalInfo.setUserName(jData.getString("userName"));
//                    personalInfo.setEmail(email = jData.getString("email"));
//                    personalInfo.setLoggedIn(true);
//                    Log.d("TAG","Username: " + userName + ", Email: " + email);
//                    if(postFunc!=null) postFunc.execute(result);
//                } catch (JSONException e) {
//                    Log.d("TAG","Failed to find userName or email in json string.");
//                }
        } else {
            Log.d("TAG","No return information from server.");
        }
    }
}

class RestDelete extends AsyncTask<String, Void, String> {
    private AsyncResponse delegate = null;

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public RestDelete(AsyncResponse delegate){
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(final String... params) {

        String REST_data = "";

        HttpURLConnection httpURLConnection = null;
        try {

            httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            httpURLConnection.setRequestMethod("DELETE");

            httpURLConnection.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(params[1]);
            Log.i("TAG",params[1]);

            wr.flush();
            wr.close();

            InputStream in = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(in);

            int inputStreamData = inputStreamReader.read();
            while (inputStreamData != -1) {
                char current = (char) inputStreamData;
                inputStreamData = inputStreamReader.read();
                REST_data += current;
            }
        } catch (Exception e) {
            Log.d("TAG","Error logging in: " + e.getMessage());
            //e.printStackTrace();
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
        }

        return REST_data;
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
}
