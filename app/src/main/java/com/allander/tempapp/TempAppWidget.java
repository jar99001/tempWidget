package com.allander.tempapp;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.RemoteViews;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Implementation of App Widget functionality.
 */
public class TempAppWidget extends AppWidgetProvider {
    static List<Double> values = new ArrayList<>();
    static Double currentTemp;
    static Integer notificationId = 0;
    static long lastUpdateTime = 0;


    void updateAppWidget(Context context, final AppWidgetManager appWidgetManager,
                                final int appWidgetId) {

        final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.temp_app_widget);
        currentTemp = 0.0;
        Log.d("WIDGET", "Running");

        new RestGet(output -> {
            try {
                JSONArray jIn = new JSONArray(output);
                for(int a=0;a<jIn.length();a++) {
                    currentTemp = jIn.getJSONObject(a).getDouble("temperatur");
                    Log.d("TMP","Temp: " + currentTemp);
                    values.add(currentTemp);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("TMP","Most recent temp: " + values.get(0));

            views.setTextViewText(R.id.appwidget_text, new DecimalFormat("#").format(values.get(0)) + "°");

            DecimalFormat tempFormat = new DecimalFormat("#.#");


            if(values.get(1) > values.get(0)) {
                views.setInt(R.id.appwidget_text, "setTextColor", Color.argb(noMore(100 + (int) (155 * ((values.get(1) - values.get(0)) / 5))), 0, 0, 255));
                if(values.get(2) > values.get(1))
                    notification(context,tempFormat.format(values.get(0)) + "°C\nTemperaturen sjunker");
                else notification(context,tempFormat.format(values.get(0)) + "°C\nStabilt");
        } else {
                views.setInt(R.id.appwidget_text, "setTextColor", Color.argb(noMore(100+(int)(155*((values.get(0)-values.get(1))/5))),255,0,0));
                if(values.get(2) < values.get(1))
                    notification(context,tempFormat.format(values.get(0)) + "°C\nTemperaturen stiger");
                else notification(context,tempFormat.format(values.get(0)) + "°C\nstabilt");
            }


            appWidgetManager.updateAppWidget(appWidgetId, views);

        }).execute("http://allander.duckdns.org/info.php?intervall=LAST");

        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    private static int noMore(int value) {
        Log.d("TMP","noMoreValue: " + value);
        if(value > 255 || value < 0) return 255;
        else return value;
    }


    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, final int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }


    private void notification(Context context, String text) {
        int hour = Calendar.getInstance().get(Calendar.HOUR);
        int min = Calendar.getInstance().get(Calendar.MINUTE);
        Log.d("TMP", hour + ":" + min);
        if(!(hour==12 && min >= 0 && min < 30)) return;
        Log.d("TMP", hour + ":" + min);


        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,"default")
                .setSmallIcon(R.drawable.thermometer)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.tempicon))
                .setContentTitle(text)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId("com.myApp");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "com.myApp",
                    "My App",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
        notificationManager.notify(notificationId,builder.build());
    }

}

